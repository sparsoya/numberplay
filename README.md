# README #

### NumberPlay ###

* NumberPlay is Java project to find lowest and second lowest number from given list of numbers. It also perform unittest
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download project 
 - ProjectFolder$ git clone 

* Configuration:
 - Maven Project
 
* Dependencies:
 - TestNG.org
 - Apache-Poi

* How to run tests
 - ProjectFolder$ mvn clean test -DsuiteXmlFile=testng.xml
